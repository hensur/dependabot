.mr: &mr
  if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
.master: &master
  if: '$CI_COMMIT_BRANCH == "master"'
.release: &release
  if: '$CI_COMMIT_TAG =~ /^v[0-9.]+$/'

.all: &all
  - <<: *mr
  - <<: *master
  - <<: *release

include:
  - local: .gitlab/ci/runners.gitlab-ci.yml

stages:
  - static analysis
  - test
  - release
  - deploy

variables:
  APP_DOCKER_IMAGE: $CI_REGISTRY_IMAGE
  GITLAB_MOCK_DOCKER_IMAGE: $CI_REGISTRY_IMAGE/gitlab-mock
  CURRENT_TAG: $CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA
  LATEST_TAG: $CI_COMMIT_REF_SLUG-latest
  CODACY_VERSION: "11.2.3"

# ======================================================================================================================
# Pre Stage
# ======================================================================================================================
cache_dependencies:
  extends: .ruby_runner
  stage: .pre
  script:
    - ./script/download-codacy.sh
  cache:
    policy: pull-push
  rules: *all

build_app_image:
  stage: .pre
  extends: .buildkit_runner
  script: |
    buildctl --addr tcp://buildkit-service.gitlab.svc.cluster.local:1234 build \
      --frontend=dockerfile.v0 \
      --local context=. \
      --local dockerfile=. \
      --opt build-arg:COMMIT_SHA=$CI_COMMIT_SHA \
      --opt build-arg:PROJECT_URL=$CI_PROJECT_URL \
      --export-cache type=inline \
      --import-cache type=registry,ref=$APP_DOCKER_IMAGE:master-latest \
      --import-cache type=registry,ref=$APP_DOCKER_IMAGE:$LATEST_TAG \
      --output type=image,\"name=$APP_DOCKER_IMAGE:$CURRENT_TAG,$APP_DOCKER_IMAGE:$LATEST_TAG\",push=true
  rules: *all

build_mock_image:
  stage: .pre
  extends: .buildkit_runner
  script: |
    buildctl --addr tcp://buildkit-service.gitlab.svc.cluster.local:1234 build \
      --frontend=dockerfile.v0 \
      --local context=spec/gitlab_mock \
      --local dockerfile=spec/gitlab_mock \
      --export-cache type=inline \
      --import-cache type=registry,ref=$GITLAB_MOCK_DOCKER_IMAGE:master-latest \
      --import-cache type=registry,ref=$GITLAB_MOCK_DOCKER_IMAGE:$LATEST_TAG \
      --output type=image,\"name=$GITLAB_MOCK_DOCKER_IMAGE:$CURRENT_TAG,$GITLAB_MOCK_DOCKER_IMAGE:$LATEST_TAG\",push=true
  rules: *all

# ======================================================================================================================
# Static analysis stage
# ======================================================================================================================
rubocop:
  extends: .ruby_runner
  stage: static analysis
  script:
    - bundle exec rubocop --parallel --color
  rules: *all

reek:
  extends: .ruby_runner
  stage: static analysis
  script:
    - bundle exec reek --color --progress --force-exclusion --sort-by smelliness .
  rules: *all

brakeman:
  extends: .ruby_runner
  stage: static analysis
  script:
    - bundle exec brakeman --color
  rules: *all

bundle_audit:
  extends: .ruby_runner
  stage: static analysis
  script:
    - bundle exec bundle-audit check --update
  rules: *all

# ======================================================================================================================
# Test Stage
# ======================================================================================================================
unit_tests:
  extends: .ruby_runner
  stage: test
  variables:
    COVERAGE: "true"
    NO_COLOR: "1"
  script:
    - bundle exec rspec --format documentation --format RspecJunitFormatter --out tmp/rspec.xml
  after_script:
    - ./codacy-coverage-reporter-$CODACY_VERSION report -r coverage/coverage.xml
  coverage: /COVERAGE:\s+(\d+.\d+)\%/
  artifacts:
    reports:
      cobertura: coverage/coverage.xml
      junit: tmp/rspec.xml
  rules: *all

e2e_test:
  image:
    name: $APP_DOCKER_IMAGE:$CURRENT_TAG
    entrypoint: [""]
  stage: test
  services:
    - name: $GITLAB_MOCK_DOCKER_IMAGE:$CURRENT_TAG
      alias: gitlab
  variables:
    SETTINGS__GITLAB_URL: http://gitlab:4567
    SETTINGS__GITLAB_ACCESS_TOKEN: e2e-test
    SETTINGS__GITHUB_ACCESS_TOKEN: $GITHUB_ACCESS_TOKEN
    RAILS_ENV: production
    GIT_STRATEGY: none
  script:
    - cd /home/dependabot
    - bundle exec rake 'dependabot:update[test-repo,bundler,/]'
  rules: *all

# ======================================================================================================================
# Release Stage
# ======================================================================================================================
release:
  extends: .docker_runner
  stage: release
  variables:
    RELEASE_IMAGE: docker.io/andrcuns/dependabot-gitlab
  script:
    - |
      apk -qq update && apk -qq add grep
      export RELEASE_VERSION=$(echo $CI_COMMIT_TAG | grep -oP 'v\K[0-9.]+')
    - |
      docker pull $APP_DOCKER_IMAGE:$CURRENT_TAG
      docker tag $APP_DOCKER_IMAGE:$CURRENT_TAG $RELEASE_IMAGE:$RELEASE_VERSION
      docker tag $APP_DOCKER_IMAGE:$CURRENT_TAG $RELEASE_IMAGE:latest
      docker push $RELEASE_IMAGE:$RELEASE_VERSION && docker push $RELEASE_IMAGE:latest
  rules:
    - *release
